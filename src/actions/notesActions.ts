import { Dispatch } from "../store";
import { Note } from "../types/Notes";
import {
  addNote,
  deleteNote,
  getNotes,
  getNotesContainingString,
  updateNote,
} from "../utils/api";

const DOMAIN = "notes";

export const ADD_NOTE_ACTION = `${DOMAIN}/add-note`;
export const ADD_NOTE_SUCCESS_ACTION = `${DOMAIN}/add-note-success`;

export const createSaveNoteAction = (note: Note) => (dispatch:Dispatch) => {
  dispatch({
    type: ADD_NOTE_ACTION,
    payload: note,
  });
  addNote(note).then(() =>
    dispatch({
      type: ADD_NOTE_SUCCESS_ACTION,
    })
  );
};

export const MOVE_NOTE_ACTION = `${DOMAIN}/move-note`;
export const MOVE_NOTE_SUCCESS_ACTION = `${DOMAIN}/move-note-success`;

export const createMoveNoteAction = (note: Note) => (dispatch:Dispatch) => {
  dispatch({
    type: MOVE_NOTE_ACTION,
    payload: note,
  });
  updateNote(note).then(() =>
    dispatch({
      type: MOVE_NOTE_SUCCESS_ACTION,
    })
  );
};

export const DELETE_NOTE_ACTION = `${DOMAIN}/delete-note`;
export const DELETE_NOTE_SUCCESS_ACTION = `${DOMAIN}/delete-note-success`;

export const createDeleteNoteAction = (id: number) => (dispatch:Dispatch) => {
  dispatch({
    type: DELETE_NOTE_ACTION,
    payload: id,
  });
  deleteNote(id).then(() =>
    dispatch({
      type: DELETE_NOTE_SUCCESS_ACTION,
    })
  );
};

export const FETCH_NOTES_ACTION = `${DOMAIN}/fetch-notes`;
export const FETCH_NOTES_SUCCESS_ACTION = `${DOMAIN}/fetch-notes-success`;

export const createFetchNotesAction = (query?: string) => (dispatch:Dispatch) => {
  dispatch({ type: FETCH_NOTES_ACTION });
  if (query)
    getNotesContainingString(query).then((notes) =>
      dispatch({
        type: FETCH_NOTES_SUCCESS_ACTION,
        payload: notes,
      })
    );
  else
    getNotes().then((notes) =>
      dispatch({
        type: FETCH_NOTES_SUCCESS_ACTION,
        payload: notes,
      })
    );
};

export const UPDATE_NOTE_ACTION = `${DOMAIN}/update-note`;
export const UPDATE_NOTE_SUCCESS_ACTION = `${DOMAIN}/update-note-success`;

export const createUpdateNoteAction = (note: Note) => (dispatch:Dispatch) => {
  dispatch({
    type: UPDATE_NOTE_ACTION,
    payload: note,
  });
  updateNote(note).then(() =>
    dispatch({
      type: UPDATE_NOTE_SUCCESS_ACTION,
    })
  );
};
