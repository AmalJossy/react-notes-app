import { Dispatch } from "../store";
import { ColorMode } from "../types/Common";
import { getLastTheme, setLastTheme } from "../utils/localStorage";

const DOMAIN = "app";
export const THEME_TOGGLE_ACTION = `${DOMAIN}/theme-toggle`;
export const createThemeToggleAction = (colormode: ColorMode) => {
  setLastTheme(colormode);
  return { type: THEME_TOGGLE_ACTION, payload: colormode };
};

export const SIDEBAR_TOGGLE_ACTION = `${DOMAIN}/sidebar-toggle`;
export const createSidebarToggleAction = (showSideBar: boolean) => {
  return { type: SIDEBAR_TOGGLE_ACTION, payload: showSideBar };
};

export const SET_ACTIVEVIEW_ACTION = `${DOMAIN}/set-activeview`;
export const createSetActiveViewAction = (activeView: string) => {
  return { type: SET_ACTIVEVIEW_ACTION, payload: activeView };
};

export const INIT_APP_ACTION = `${DOMAIN}/init-app`;
export const createInitAppAction = () => (dispatch:Dispatch) => {
  const colormode = getLastTheme() ?? "light";
  console.log({colormode})
  dispatch({ type: THEME_TOGGLE_ACTION, payload: colormode });
  return { type: INIT_APP_ACTION };
};
