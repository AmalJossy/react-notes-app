export type Note ={
    id?:number;
    title?:string;
    description?:string;
    tags?: string[];
    color?: string;
    archived?: boolean;
    pinned?: boolean;
}