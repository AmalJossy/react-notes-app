export type ColorMode = "light" | "dark";
export type ActiveView = "Notes" | "Archive"