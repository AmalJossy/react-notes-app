import { ReactNode } from "react";
import {  Moon, Sun } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { createThemeToggleAction } from "../../actions/appActions";
import { selectColorMode } from "../../selectors/appSelectors";
import { ColorMode } from "../../types/Common";
import styles from "./ThemeToggle.module.css";
type Theme = {
  nextTheme: ColorMode,
  nextThemeIcon: ReactNode,
  nextThemeHint: string
}
const themeInfo: { [key: string]: Theme } = {
  light: {
    nextTheme: "dark",
    nextThemeIcon: <Moon height={16} width={16}/>,
    nextThemeHint: "Use Dark theme",
  },
  dark: {
    nextTheme: "light",
    nextThemeIcon: <Sun height={16} width={16} />,
    nextThemeHint: "Use Light theme",
  },
};

const ThemeToggle = () => {
  const colorMode = useSelector(selectColorMode);
  const dispatch = useDispatch();
  const handleToggle: React.MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    dispatch(createThemeToggleAction(themeInfo[colorMode].nextTheme));
  };
  return (
    <button
      className={styles.ThemeToggle}
      title={themeInfo[colorMode].nextThemeHint}
      onClick={handleToggle}
    >
      {themeInfo[colorMode].nextThemeIcon}
    </button>
  );
};

export default ThemeToggle;
