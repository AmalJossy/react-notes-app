import { useState } from "react";
import { Note } from "../../types/Notes";
import { useDispatch } from "react-redux";
import NoteEditor from "../../components/NoteEditor";
import { createSaveNoteAction } from "../../actions/notesActions";

const initialNote: Note = { title: "", description: "" };

function CreateBox() {
  const dispatch = useDispatch();
  const [note, setNote] = useState(initialNote);

  const saveNote = (note:Note) => {
    dispatch(createSaveNoteAction(note));
    setNote({...initialNote})  // reset editor to initial value
  };
  const deleteNote = () => {
    setNote({...initialNote}) // reset editor to initial value, since note not saved yet
  }
  return (
    <NoteEditor note={note} onSave={saveNote} onDelete={deleteNote}/>
  );
}

export default CreateBox;
