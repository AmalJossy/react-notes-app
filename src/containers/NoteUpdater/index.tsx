import { useDispatch } from "react-redux";
import {
  createDeleteNoteAction,
  createUpdateNoteAction,
} from "../../actions/notesActions";
import NoteEditor from "../../components/NoteEditor";
import { Note } from "../../types/Notes";
import history from "../../utils/history";

type NoteUpdaterProps = {
  note: Note;
};

const NoteUpdater = (props: NoteUpdaterProps) => {
  const { note } = props;
  const dispatch = useDispatch();
  const saveHandler = (note: Note) => {
    dispatch(createUpdateNoteAction(note));
    history.push("/");
  };
  const deleteHandler = (note: Note) => {
    if (note?.id) dispatch(createDeleteNoteAction(note.id));
    history.push("/");
  };
  return (
    <NoteEditor
      note={note}
      onSave={saveHandler}
      onDelete={deleteHandler}
      fullwidth
    />
  );
};

export default NoteUpdater;
