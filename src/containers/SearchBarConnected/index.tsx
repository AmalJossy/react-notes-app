import { useParams } from "react-router-dom";
import history from "../../utils/history";

import SearchBar from "../../components/SearchBar";

function SearchBarConnected() {
  const { query: queryToken } = useParams<{ query: string }>();

  return (
    <SearchBar
      query={queryToken}
      onSearch={(query) => {
        history.push(`/search/${query}`);
      }}
      onClear={() => {
        history.push(`/`);
      }}
    />
  );
}

export default SearchBarConnected;
