import { Menu } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { createSidebarToggleAction } from "../../actions/appActions";
import { selectIsSidebarExpanded } from "../../selectors/appSelectors";
import styles from "./SidebarToggle.module.css";
function SideBarToggle() {
  const dispatch = useDispatch();
  const sidebarExpanded = useSelector(selectIsSidebarExpanded);
  const sidebarToggleHandler: React.MouseEventHandler<HTMLButtonElement> = (
    e
  ) => {
    e.preventDefault();
    dispatch(createSidebarToggleAction(!sidebarExpanded));
  };
  return (
    <button
      className={styles.SideBarToggle}
      aria-pressed={sidebarExpanded}
      onClick={sidebarToggleHandler}
    >
      <Menu />
    </button>
  );
}
export default SideBarToggle;
