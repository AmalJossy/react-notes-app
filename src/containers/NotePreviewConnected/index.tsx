import { useDispatch } from "react-redux";
import { createDeleteNoteAction, createMoveNoteAction } from "../../actions/notesActions";
import NotePreview from "../../components/NotePreview";
import { Note } from "../../types/Notes";

function NotePreviewConnected(props: { note: Note }) {
  const dispatch = useDispatch();
  const handlePinChange = (pinned:boolean) => {
    dispatch(createMoveNoteAction({...props.note, pinned}))
  };
  const handleArchiveChange = (archived:boolean) => {
    dispatch(createMoveNoteAction({...props.note, archived}))
  };
  const handleDelete = (id:number) => {
    dispatch(createDeleteNoteAction(id))
  };
  return <NotePreview note={props.note} onPinChange={handlePinChange} onArchiveChange={handleArchiveChange} onDelete={handleDelete}/>;
}

export default NotePreviewConnected;
