import { useRef } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useRouteMatch } from "react-router-dom";
import { createFetchNotesAction } from "../../actions/notesActions";
import Library from "../../components/Library/Index";
import {
  selectIsNotesStateStale,
  selectNotes,
} from "../../selectors/notesSelectors";
import { ActiveView } from "../../types/Common";
import CreateBox from "../CreateBox";
import styles from "./Display.module.css";

function Display() {
  const notes = useSelector(selectNotes);
  const isStale = useSelector(selectIsNotesStateStale);
  // const activeView: string = useSelector(selectActiveView);
  const activeView: ActiveView = useRouteMatch("/archive")?.isExact
    ? "Archive"
    : "Notes";

  const dispatch = useDispatch();

  const { query } = useParams<{ query: string }>();
  const queryRef = useRef("");
  useEffect(() => {
    if (isStale || query !== queryRef.current) { // fetch fresh data only on stale or query change
      dispatch(createFetchNotesAction(query));
      queryRef.current = query;
    }
  }, [dispatch, isStale, query]);

  return (
    <div className={styles.DisplayArea}>
      {activeView === "Archive" || (
        <div className={styles.CreateBoxContainer}>
          <CreateBox />
        </div>
      )}
      <Library
        notes={notes.filter(
          (note) =>
            (activeView === "Archive" ? note.archived : !note.archived) || query
        )}
      />
    </div>
  );
}

export default Display;
