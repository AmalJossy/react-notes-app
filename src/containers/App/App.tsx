import Display from "../Display";
import Header from "../../components/Header";
import SideBar from "../../components/SideBar";
import styles from "./App.module.css";
import themes from "./Theme.module.css"; //isolating variables for better maintenance
import { useDispatch, useSelector } from "react-redux";
import {
  selectColorMode,
  selectIsSidebarExpanded,
} from "../../selectors/appSelectors";
import { ActiveView } from "../../types/Common";
import Modal from "../../components/Modal";
import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { getNoteById } from "../../utils/api";
import { Note } from "../../types/Notes";
import NoteFullView from "../../components/NoteFullView";
import history from "../../utils/history";
import { createInitAppAction } from "../../actions/appActions";

type AppProps = {
  view?: ActiveView;
};
function App(props: AppProps) {
  const { view } = props;
  const [noteToView, setNoteToView] = useState<Note | undefined>();
  const colorMode = useSelector(selectColorMode);
  const showSidebar = useSelector(selectIsSidebarExpanded);
  // const activeView: string = useSelector(selectActiveView);
  const activeView = view ?? "Notes";
  const { noteId } = useParams<{ noteId: string }>();
  useEffect(() => {
    if (noteId)
      getNoteById(parseInt(noteId)).then((note) => {
        if (note) setNoteToView(note);
      });
    else {
      setNoteToView(undefined);
    }
  }, [noteId]);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(createInitAppAction());// eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); // run only once 

  return (
    <div className={`${styles.App} ${themes[colorMode]}`}>
      <Header title={activeView} />
      <main className={styles.MainArea}>
        <div
          className={`${styles.ScrollableContainer} ${styles.SidebarContainer}`}
        >
          <SideBar expanded={showSidebar} />
        </div>
        <div
          className={`${styles.ScrollableContainer} ${styles.DisplayContainer}`}
        >
          <Display />
        </div>
      </main>
      {noteToView && (
        <Modal show={true} onClose={() => history.push("/")}>
          <NoteFullView note={noteToView} />
        </Modal>
      )}
    </div>
  );
}

export default App;
