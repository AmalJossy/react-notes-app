import { combineReducers } from "redux";
import { appReducer, AppState } from "./appReducer";
import { notesReducer, NotesState } from "./notesReducer";

export type State = {
  app: AppState;
  notesState: NotesState;
};

const rootReducer = combineReducers({
  app: appReducer,
  notesState: notesReducer,
});
export default rootReducer;
