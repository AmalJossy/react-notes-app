import { AnyAction } from "redux";
import {
  ADD_NOTE_SUCCESS_ACTION,
  DELETE_NOTE_SUCCESS_ACTION,
  FETCH_NOTES_SUCCESS_ACTION,
  MOVE_NOTE_SUCCESS_ACTION,
  UPDATE_NOTE_SUCCESS_ACTION,
} from "../actions/notesActions";

import { Note } from "../types/Notes";

export type NotesState = {
  notes: Note[];
  stale: boolean;
};
const initialState: NotesState = {
  notes: [],
  stale: true,
};

export const notesReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case FETCH_NOTES_SUCCESS_ACTION:
      return { ...state, notes: action.payload, stale: false };

    /** Allow multiple actions to make state stale for easier debugging.
     *  Also some cases may need to be handled specially later **/
    case ADD_NOTE_SUCCESS_ACTION:
      return { ...state, stale: true };
    case UPDATE_NOTE_SUCCESS_ACTION:
      return { ...state, stale: true };
    case MOVE_NOTE_SUCCESS_ACTION:
      return { ...state, stale: true };
    case DELETE_NOTE_SUCCESS_ACTION:
      return { ...state, stale: true };
      
  }
  return state;
};
