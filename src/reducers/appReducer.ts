import { AnyAction } from "redux";
import { ColorMode } from "../types/Common";
import { SET_ACTIVEVIEW_ACTION, SIDEBAR_TOGGLE_ACTION, THEME_TOGGLE_ACTION } from "../actions/appActions";

export type AppState = {
  colorMode: ColorMode;
  showSidebar: boolean;
  activeView: "Notes" | "Archive";
};
const initialState: AppState = {
  colorMode: "dark",
  showSidebar: false,
  activeView: "Notes"
};

export const appReducer = (state = initialState, action: AnyAction):AppState => {
  switch (action.type) {
    case THEME_TOGGLE_ACTION:
      return { ...state, colorMode: action.payload };
    case SIDEBAR_TOGGLE_ACTION:
      return { ...state, showSidebar: action.payload };
      case SET_ACTIVEVIEW_ACTION:
        return { ...state, activeView: action.payload };
  }
  return state;
};
