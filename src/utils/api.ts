import { IDBPDatabase, openDB } from "idb";
import { Note } from "../types/Notes";

const DB_NAME = "keep-db";
const DB_VERSION = 1;
const NOTE_STORE = "notes";

interface KeepDB {
  notes: {
    value: Note;
    key: number;
  };
}

let db: IDBPDatabase<KeepDB>;

export const connect = async () => {
  db = await openDB<KeepDB>(DB_NAME, DB_VERSION, {
    upgrade(db) {
      db.createObjectStore(NOTE_STORE, {
        keyPath: "id",
        autoIncrement: true,
      });
    },
  });
};
export const addNote = async (note: Note) => {
  if (!db) await connect();
  return await db.add(NOTE_STORE, note);
};

export const getNotes = async (limit: number = 25, offset: number = 0) => {
  if (!db) await connect();
  return (await db.getAll(NOTE_STORE)).sort(
    (noteA, noteB) => noteB.id - noteA.id
  );
};
export const getNotesContainingString = async (query: string) => {
  if (!db) await connect();
  // TODO do actual query
  return (await db.getAll(NOTE_STORE)).filter(
    (note) => note.title?.includes(query) || note.description?.includes(query)
  );
};

export const getNoteById = async (id: number) => {
  if (!db) await connect();
  return await db.get(NOTE_STORE, id);
};

export const updateNote = async (note: Note) => {
  if (!db) await connect();
  return await db.put(NOTE_STORE, note);
};

export const deleteNote = async (id: number) => {
  if (!db) await connect();
  return await db.delete(NOTE_STORE, id);
};
