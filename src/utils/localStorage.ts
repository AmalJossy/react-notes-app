import { ColorMode } from "../types/Common";

const LAST_THEME_KEY = "last-theme-key";

export const setLastTheme = (theme: ColorMode) => {
  window.localStorage.setItem(LAST_THEME_KEY, theme);
};

export const getLastTheme = () => {
  return (window.localStorage.getItem(LAST_THEME_KEY) as ColorMode|null);
};
