import { Selector } from "react-redux";
import { State } from "../reducers";
import { ActiveView, ColorMode } from "../types/Common";

/// TODO: consider reselect
export const selectColorMode: Selector<State, ColorMode> = (state) =>
  state.app.colorMode;

export const selectIsSidebarExpanded: Selector<State, boolean> = (state) =>
  state.app.showSidebar;

export const selectActiveView: Selector<State, ActiveView> = (state) =>
  state.app.activeView;
