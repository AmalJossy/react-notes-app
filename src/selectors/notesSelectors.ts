import { State } from "../reducers";

export const selectNotes = (state: State) => {
    return state.notesState.notes;
  };
  export const selectIsNotesStateStale= (state: State) => {
    return state.notesState.stale;
  };
  