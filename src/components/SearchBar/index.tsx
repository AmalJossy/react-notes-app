import styles from "./SearchBar.module.css";
import React, { useEffect, useState } from "react";
import { Search, X } from "react-feather";

type SearchBarProps = {
  query?: string;
  onSearch: (arg0: string) => void;
  onClear?: (arg0?: string) => void;
  onChange?: (arg0: string) => void;
};

function SearchBar(props: SearchBarProps) {
  const { query: _query, onSearch, onChange, onClear } = props;
  const [query, setQuery] = useState("");

  const changeHandler = (query: string) => {
    setQuery(query);
    onChange?.(query);
  };

  const cancelHandler = () => {
    onClear?.(query);
    setQuery("");
  };

  const searchHandler: React.MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault(); // stop form submit from refreshing page
    onSearch(query);
  };

  useEffect(() => {
    if (_query) setQuery(_query);
  }, [_query]);

  return (
    <form>
      <div className={styles.SearchBar}>
        <button
          className={styles.SearchButton}
          type="submit"
          onClick={searchHandler}
          title="Search"
        >
          <Search height={20} width={20} />
        </button>
        <div className={styles.SearchInputContainer}>
          <input
            className={styles.SearchInput}
            type="text"
            placeholder="Search"
            value={query}
            onChange={(e) => changeHandler(e.target.value)}
          />
        </div>
        <button
          className={`${styles.CloseButton} ${query ? styles.Visible : ""}`}
          type="reset"
          title="Cancel"
          onClick={cancelHandler}
        >
          <X  height={20} width={20}/>
        </button>
      </div>
    </form>
  );
}

export default SearchBar;
