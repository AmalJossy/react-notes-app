import ReactMarkdown from "react-markdown";
import { Note } from "../../types/Notes";
import styles from "./NotePreview.module.css";
import { Link } from "react-router-dom";
import { Archive, Flag, Trash } from "react-feather";

type NotePreviewProps = {
  note: Note;
  onPinChange: (arg0: boolean) => void;
  onArchiveChange: (arg0: boolean) => void;
  onDelete: (arg0: number) => void;
};

function NotePreview(props: NotePreviewProps) {
  const {
    onPinChange,
    onArchiveChange,
    onDelete,
    note: { id, title, description, pinned, archived, tags },
  } = props;

  return (
    <div className={styles.NoteWrapper}>
      <Link to={`/view/${id}`}>
        <article key={id} className={styles.Note}>
          <header>
            <h4 className={styles.Title}>{title}</h4>
          </header>
          <div className={styles.PreviewContainer}>
            <ReactMarkdown className={styles.Description}>
              {description || ""}
            </ReactMarkdown>
          </div>
          <footer>
            {tags?.map((tag) => (
              <span className={styles.Tag} key={tag}>
                {tag}
              </span>
            ))}
          </footer>
        </article>
      </Link>
      <div className={styles.Actions}>
          <button
            aria-pressed={archived}
            onClick={() => {
              onArchiveChange(!archived);
            }}
            title={`${archived ? "Unarchive" : "Archive"} this note`}
          >
            <Archive height={16} width={16} />
          </button>
          <button
            aria-pressed={pinned}
            onClick={() => {
              onPinChange(!pinned);
            }}
            title={`${pinned ? "Unpin" : "Pin"} this note`}
          >
            <Flag height={16} width={16} />
          </button>
          <button onClick={() => id && onDelete(id)} title="Delete Note">
            <Trash height={16} width={16} />
          </button>
      </div>
    </div>
  );
}

export default NotePreview;
