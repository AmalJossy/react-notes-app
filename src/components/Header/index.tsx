import styles from "./Header.module.css";
import Title from "../Title";
import SearchBarConnected from "../../containers/SearchBarConnected";
import ThemeToggle from "../../containers/ThemeToggle";
import SideBarToggle from "../../containers/SidebarToggle";

type HeaderProps = {
  title: string;
};
function Header(props: HeaderProps) {
  return (
    <div className={styles.Header}>
      <div className={styles.Navigation}>
        <SideBarToggle />
        <Title title={props.title} />
      </div>
      <div className={styles.SearchContainer}>
        <SearchBarConnected />
      </div>
      <div className={styles.ToggleContainer}>
        <ThemeToggle />
      </div>
    </div>
  );
}

export default Header;
