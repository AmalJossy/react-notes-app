import NotePreviewConnected from "../../containers/NotePreviewConnected";
import { Note } from "../../types/Notes";
import styles from "./Library.module.css";

type LibraryRackProps = {
  notes: Note[];
};

function LibraryRack(props: LibraryRackProps) {
  return (
    <div className={styles.LibraryRack}>
      {props.notes.map((note) => (
        <NotePreviewConnected note={note} key={note.id} />
      ))}
    </div>
  );
}

export default LibraryRack;
