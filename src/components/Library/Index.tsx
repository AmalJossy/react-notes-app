import { Note } from "../../types/Notes";
import styles from "./Library.module.css";
import LibraryRack from "./LibraryRack";

type LibraryProps = {
  notes: Note[];
};

function Library(props: LibraryProps) {
  const archived = props.notes.filter((note) => note.archived);
  const pinned = props.notes.filter((note) => note.pinned && !note.archived);
  const others = props.notes.filter((note) => !note.archived && !note.pinned);
  return (
    <div className={styles.Library}>
      {pinned.length > 0 && (
        <div>
          <h3>{"Pinned"}</h3>
          <hr className={styles.HRule} />
          <LibraryRack notes={pinned} />
        </div>
      )}
      {archived.length > 0 && (
        <div>
          <h3>{"Archived"}</h3>
          <hr className={styles.HRule} />
          <LibraryRack notes={archived} />
        </div>
      )}
      {others.length > 0 && (
        <div>
          <h3>{"Notes"}</h3>
          <hr className={styles.HRule} />
          <LibraryRack notes={others} />
        </div>
      )}
    </div>
  );
}

export default Library;
