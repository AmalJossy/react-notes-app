import ReactMarkdown from "react-markdown";

import styles from "./NoteEditor.module.css";
import { useEffect, useState } from "react";
import { Note } from "../../types/Notes";
import { Archive, Eye, Flag, Trash } from "react-feather";

type NoteEditorProps = {
  onSave?: (arg0: Note) => void;
  onDelete?: (arg0: Note) => void;
  note: Note;
  fullwidth?:boolean;
};

function NoteEditor(props: NoteEditorProps) {
  const { onSave, onDelete, note: _note, fullwidth } = props;

  const [note, setNote] = useState<Note>({});
  const [isPreview, setIsPreview] = useState(false);

  useEffect(() => {
    setNote(_note);
  }, [_note]);

  const handleSave = () => {
    if (note.description || note.title) onSave?.(note);
  };
  return (
    <div className={`${styles.NoteEditor} ${fullwidth ? styles.FullWidth : ""}`}>
      <div>
        <input
          className={styles.Title}
          type="text"
          placeholder="Title"
          value={note.title || ""}
          onChange={(e) => {
            setNote({ ...note, title: e.target.value });
          }}
        />
      </div>
      <div>
        {isPreview ? (
          <ReactMarkdown className={styles.Preview}>{note.description || ""}</ReactMarkdown>
        ) : (
          <textarea
            className={styles.Description}
            placeholder="Take a note.."
            onChange={(e) => {
              setNote({ ...note, description: e.target.value });
            }}
            value={note.description}
          />
        )}
      </div>
      <div className={styles.Actions}>
        <div>
          <button
            aria-pressed={note.archived}
            onClick={() => setNote({ ...note, archived: !note.archived })}
            title="Toggle Pinned"
          >
            <Archive height={18} width={18}/>
          </button>
          <button
            aria-pressed={note.pinned}
            onClick={() => setNote({ ...note, pinned: !note.pinned })}
            title="Toggle Archived"
          >
            <Flag height={18} width={18} />
          </button>
          <button
            aria-pressed={isPreview}
            onClick={() => setIsPreview(!isPreview)}
            title="Toggle Preview"
          >
            <Eye height={18} width={18} />
          </button>
          <button
            onClick={() => onDelete?.(note)}
            title="Delete Note"
          >
            <Trash height={18} width={18} />
          </button>
        </div>
        <div>
          <button
            onClick={handleSave}
            title="Save and Close"
            disabled={!(note.title || note.description)}
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
}

export default NoteEditor;
