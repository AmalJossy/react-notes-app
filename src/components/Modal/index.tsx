import { ReactNode } from "react";
import { X } from "react-feather";
import styles from "./Modal.module.css";

type ModalProps = {
  children: ReactNode;
  show?: boolean;
  onClose?: () => void;
};
function Modal(props: ModalProps) {
  const { children, show, onClose } = props;
  if (!show) return null;
  return (
    <div className={styles.Backdrop}>
      <div className={styles.Modal}>
        {children}
        <button onClick={() => onClose?.()} className={styles.CloseButton}>
          <X height={16} width={16} />
        </button>
      </div>
    </div>
  );
}

export default Modal;
