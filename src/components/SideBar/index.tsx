import styles from "./SideBar.module.css";
import { Link } from "react-router-dom";
import { Archive, FileText } from "react-feather";

type SideBarProps = { expanded: boolean };

function SideBar(props: SideBarProps) {
  return (
    <nav className={styles.Menu}>
      <Link to="/" className={styles.NavItem} title="Home">
        <FileText />
        {props.expanded ? <p className={styles.Label}>{"Notes"}</p> : null}
      </Link>
      <Link to="/archive" className={styles.NavItem} title="Archived notes">
        <Archive />
        {props.expanded ? <p className={styles.Label}>{"Archived"}</p> : null}
      </Link>
    </nav>
  );
}

export default SideBar;
