import NoteUpdater from "../../containers/NoteUpdater";
import { Note } from "../../types/Notes";
import styles from "./NoteFullView.module.css";

type NoteFullViewProps = { note: Note };

function NoteFullView(props: NoteFullViewProps) {
  const { note } = props;
  return (
    <div className={styles.ScrollableContainer}>
      <NoteUpdater note={note}/>
    </div>
  );
}

export default NoteFullView;
