import styles from "./Title.module.css";

type TitleProps = { title: string };

function Title(props: TitleProps) {
  return <h2 className={styles.Title}>{props.title}</h2>;
}

export default Title;
