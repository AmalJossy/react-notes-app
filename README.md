
# Google Keep Clone

A mock-frontend that emulates basic functionalities of google keep
## Installation 

Install with yarn

```bash 
  yarn install 
```
    
## Commands

To start this project in development mode run

```bash
  yarn start
```

or

```bash
  npm run start
```

  
## Features

- Light/dark mode toggle (toggle on top-right)
- Notes persist on page refresh (uses indexDB for now)
- Search in notes title or description (Click 🔍or press Enter, X to clear )
- Responsive (layout chnages at 600px device-width)
- Create Update or Delete Notes
- Archive or Pin Notes
- Markdown support and image support via markdown